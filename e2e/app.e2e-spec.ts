import { PolygonalchainPage } from './app.po';

describe('polygonalchain App', () => {
  let page: PolygonalchainPage;

  beforeEach(() => {
    page = new PolygonalchainPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
